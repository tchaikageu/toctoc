# Toctoc

Simple cli ports knocking tool

*toc toc toc* is the french expression for *knock knock*


## Installation instruction

```
git clone https://git.envs.net/tchaikageu/toctoc.git toctoc
cd toctoc
nimble install
```

## Usage

```
Usage:
  toctoc [options] host [ports ...]

Arguments:
  host             Host to knock
  [ports ...]      port,protocol (ex 123,TCP 234,UDP)

Options:
  -h, --help
  -v, --verbose              Verbose
  -d, --delay=DELAY          Delay in milliseconds (default: 200)
  -t, --timeout=TIMEOUT      Timeout in milliseconds (default: 200)
```

## Usage example

```
toctoc --verbose --timeout=300 --delay=500 myhost.com 123,TCP 234,UDP 345,TCP
```
Can be shorten to:
```
toctoc -v -t 300 -d 500 myhost.com 123 234,UDP 345
```

