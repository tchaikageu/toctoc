import std/net
import std/strutils
import argparse

type Target* = ref object
  proto: Protocol
  port: Port
  timeout: int


proc quitOnError(msg: string) =
  echo(msg)
  quit(1)


proc knockPort*(host: string, target: Target) =
  try:
    if target.proto == IPPROTO_TCP:
      let socket = newSocket(protocol = target.proto)
      socket.connect(host, port = target.port, timeout = target.timeout)
    else:
      let socket = newSocket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)
      socket.sendTo(host, target.port, "status\n")
  except OSError as e:
    quitOnError("OSError: " & e.msg)
  except TimeOutError:
    discard


proc getIntValue*(option: string): int =
  try:
    result = parseInt(option)
  except ValueError:
    quitOnError(option & " is not a int")


proc parsePortsOpts*(ports: seq[string], timeout: int): seq[Target] =
  if len(ports) == 0:
    quitOnError("You must provide ports to knock")
  for i in ports:
    var proto: Protocol = IPPROTO_TCP
    let target = split(i, ',')
    let port = Port(getIntValue(target[0]))
    if target.len == 2:
      case toUpperAscii(target[1])
      of "TCP": discard
      of "UDP": proto = IPPROTO_UDP
      else:
        quitOnError("Wrong protocol: " & target[1] &
        "\nAvaible protocols are: TCP or UDP")
    result.add(Target(port: port, proto: proto, timeout: timeout))


when isMainModule:
  let p = newParser("toctoc"):
    help("Simple ports knocking app")
    flag("-v", "--verbose", help = "Verbose")
    option("-d", "--delay", help = "Delay in milliseconds", default = some("200"))
    option("-t", "--timeout", help = "Timeout in milliseconds", default = some("200"))
    arg("host", help = "Host to knock", nargs = 1)
    arg("ports", help = "port,protocol (ex 123,TCP 234,UDP)", nargs = -1)
  try:
    let opts = p.parse()
    let delay = getIntValue(opts.delay)
    let timeout = getIntValue(opts.timeout)
    let targetHost = opts.host
    let knockTargets = parsePortsOpts(opts.ports, timeout)

    if opts.verbose: echo "Host: ", targetHost
    var index = 0
    for target in knockTargets:
      if opts.verbose:
        echo "Knocking port ", target.port
      knockPort(targetHost, target)
      index += 1
      if index < knockTargets.len():
        sleep(delay)

  except:
    echo p.help
    echo("Usage example:")
    echo("toctoc --verbose --delay=500 --timeout=300 myhost.org 123,TCP 234,UDP 345,TCP\n")
    echo("Short version:")
    echo("toctoc -v -d 300 -t 500 myhost.org 123 234,UDP 345")
