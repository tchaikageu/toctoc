# Package

version = "0.1.1"
author = "Tchaikageu"
description = "Simple cli app for port knocking"
license = "WTFTPL"
srcDir = "src"
bin = @["toctoc"]

import strformat


let opts = &"--verbose -d:lto -d:version=\"{version}\""


# Dependencies

requires "nim >= 1.6"
requires "argparse"

task danger, "Build for danger":
  exec &"nimble build -d:danger -d:strip --opt:size {opts}"

task release, "Build for release":
  exec &"nimble build -d:release -d:strip --opt:size {opts}"

task debug, "Build for debug":
  exec &"nimble build -d:debug --debugger:native -y --styleCheck:hint {opts}"
